from django import forms
from django.contrib.auth.hashers import make_password
from django.forms import ModelForm

from accounts.models import Profile


class UserForm(ModelForm):
    class Meta:
        model = Profile
        fields = "__all__"
        widgets = {
            "username": forms.TextInput(attrs={'placeholder': '@username'}),
            "first_name": forms.TextInput(attrs={'placeholder': 'First Name'}),
            "last_name": forms.TextInput(attrs={'placeholder': 'Last Name'}),
            "email": forms.TextInput(attrs={'placeholder': 'Email Address'}),
            "password": forms.PasswordInput(attrs={'placeholder': 'Password'})
        }

    def save(self, commit=True, *args, **kwargs):
        m = super().save(commit=False)
        m.password = make_password(self.cleaned_data.get('password'))

        if commit:
            m.save()
        return m
