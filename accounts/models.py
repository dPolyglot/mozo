from django.core.validators import *
from django.db import models


# Create your models here.
class Profile(models.Model):
    username = models.CharField(max_length=255, validators=[MaxLengthValidator(10)], unique=True,verbose_name='')
    first_name = models.CharField(max_length=255,verbose_name='')
    last_name = models.CharField(max_length=255,verbose_name='')
    email = models.EmailField(verbose_name='')
    password = models.CharField(max_length=60,verbose_name='')

    def __str__(self):
        return self.username
