from django.urls import path
from . import views

urlpatterns = [
    path('register', views.register, name="register"),
    path('login', views.login_user, name="login"),
    path('recover', views.recover, name="recover"),
    path('dashboard', views.user_dashboard, name="dashboard")
]
