from django.contrib.auth import authenticate
from django.shortcuts import render, redirect

# Create your views here.
from django.views.decorators.csrf import csrf_protect

from .forms import UserForm


def register(request):
    if request.method == 'POST':
        register_form = UserForm(request.POST)

        if register_form.is_valid():
            register_form.save()
            return redirect('movies')
    else:
        register_form = UserForm()
    return render(request, 'register.html', {'form': register_form})


def login_user(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)

        if user is not None:
            return redirect('dashboard')

    return render(request, 'login.html')


def user_dashboard(request):
    return render(request, 'dashboard.html')


def recover(request):
    return render(request, 'recover.html')
